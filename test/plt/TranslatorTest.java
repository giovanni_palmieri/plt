package plt;

import static org.junit.Assert.*;

import org.junit.Test;

public class TranslatorTest {

	@Test
	public void testInputPhrase() {
		String inputPhrase = "hello world";
		Translator translator = new Translator(inputPhrase);
		assertEquals("hello world", translator.getPhrase());
	}
	
	@Test
	public void testTranslationEmptyPhrase() {
		String inputPhrase = "";
		Translator translator = new Translator(inputPhrase);
		assertEquals(Translator.NIL, translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithVowelEndingWithY() {
		String inputPhrase = "any";
		Translator translator = new Translator(inputPhrase);
		assertEquals("anynay", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithUEndingWithY() {
		String inputPhrase = "utility";
		Translator translator = new Translator(inputPhrase);
		assertEquals("utilitynay", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithVowelEndingWithVowel() {
		String inputPhrase = "apple";
		Translator translator = new Translator(inputPhrase);
		assertEquals("appleyay", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithVowelEndingWithConsonant() {
		String inputPhrase = "ask";
		Translator translator = new Translator(inputPhrase);
		assertEquals("askay", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithSingleConsonant() {
		String inputPhrase = "hello";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithTwoConsonant() {
		String inputPhrase = "known";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ownknay", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithMultipleConsonant() {
		String inputPhrase = "scrap";
		Translator translator = new Translator(inputPhrase);
		assertEquals("apscray", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseWithMultipleWordsSeparatedBySpace() {
		String inputPhrase = "hello world";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay orldway", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseWithMultipleWordsSeparatedByDash() {
		String inputPhrase = "well-being";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellway-eingbay", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseWithMultipleWordsContainingCompositeWord() {
		String inputPhrase = "hello world well-being";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay orldway ellway-eingbay", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseContainingPuntuaction() {
		String inputPhrase = "hello world!";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay orldway!", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseContainingMultiplePuntuactions() {
		String inputPhrase = "hello world! (well-being).";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay orldway! (ellway-eingbay).", translator.translate());
	}

}
