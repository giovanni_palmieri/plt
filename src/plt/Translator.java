package plt;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

public class Translator {

	public static final String NIL = "nil";
	private String phrase;
	
	public Translator(String inputPhrase) {
		phrase = inputPhrase;
	}

	public String getPhrase() {
		return phrase;
	}

	public String translate() {
		String[] words = phrase.split("\\s+");
		ArrayList<String> translatedWords = new ArrayList<>();
		
		for (String word : words) {
			translatedWords.add(preservePuntuaction(word));
		}
		
		if(!translatedWords.isEmpty()) {
			StringBuilder builder = new StringBuilder();
			for(String word : translatedWords) {
				builder.append(word + " ");
			}
			return builder.toString().trim();
		}
		
		return NIL;
	}
	
	private boolean startWithVowel(String word) {
		return word.startsWith("a") || word.startsWith("e") || word.startsWith("i") || word.startsWith("o") ||word.startsWith("u");
	}

	private boolean endWithVowel(String word) {
		return word.endsWith("a") || word.endsWith("e") || word.endsWith("i") || word.endsWith("o") ||word.endsWith("u");
	}
	
	private int countConsonantsAtBeginning(String word) {
		int consonantCounter = 0;
		for (int i = 0; i < word.length(); i++) {
			if("aeiou".indexOf(word.charAt(i)) == -1) {
				consonantCounter++;
			} else {
				break;
			}
		}
		return consonantCounter;
	}

	private String translateWord(String word) {
		if(isCompositeWord(word)) {
			return translateCompositeWord(word);
		} else if(startWithVowel(word)) {
			if(word.endsWith("y")) {
				return word + "nay";
			} else if(endWithVowel(word)) {
				return word + "yay";
			} else {
				return word + "ay";
			}
		} else if(countConsonantsAtBeginning(word) == 1) {
			return word.substring(1) + word.charAt(0) + "ay";
		} else if(countConsonantsAtBeginning(word) > 1) {
			int consonants = countConsonantsAtBeginning(word);
			return word.substring(consonants) + word.substring(0, consonants) +"ay";
		} 
		return NIL;
	}
	
	private boolean isCompositeWord(String word) {
		return word.contains("-");
	}
	
	private String translateCompositeWord(String word) {
		String[] splittedWord = word.split("-");
		return translateWord(splittedWord[0]) + "-" + translateWord(splittedWord[1]);
	}
	
	private String preservePuntuaction(String word) {
		ArrayList<Character> puntuactionCharacters = new ArrayList<>();
		ArrayList<Integer> puntuactionCharactersPos = new ArrayList<>();
		
		for(int i = 0; i < word.length(); i++) {
			char x = word.charAt(i);
			int index = ".,;:?!'()".indexOf(x);
			if(index != -1) {
				puntuactionCharacters.add(word.charAt(i));
				puntuactionCharactersPos.add(i);
			}
		}
		
		if(!puntuactionCharacters.isEmpty()) {
			String wordNoPunctuation = word.replaceAll("[.,;:?!'()]", ""); 
			StringBuilder builder = new StringBuilder(translateWord(wordNoPunctuation));
			for(int i = 0; i < puntuactionCharacters.size(); i++) {
				if(puntuactionCharactersPos.get(i) + 1 > wordNoPunctuation.length()) {
					builder.append(puntuactionCharacters.get(i));		
				} else {
					builder.insert(0, puntuactionCharacters.get(i));	
				}
			}
			return builder.toString();
		} else {
			return translateWord(word);
		}
		
	}
}
